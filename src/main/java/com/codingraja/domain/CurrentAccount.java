package com.codingraja.domain;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("current_account")
public class CurrentAccount extends Account {
	@Column(name="mab")
	private Double mab;  // Monthly Average Balance
	@Column(name="max_txn_limit")
	private Long maxTxn; // Max Transaction Limit
	
	public CurrentAccount(){}
	public CurrentAccount(Double balance, Long customerId, 
								 Double mab, Long maxTxn) {
		super(balance, customerId);
		this.mab = mab;
		this.maxTxn = maxTxn;
	}

	public Double getMab() {
		return mab;
	}

	public void setMab(Double mab) {
		this.mab = mab;
	}

	public Long getMaxTxn() {
		return maxTxn;
	}

	public void setMaxTxn(Long maxTxn) {
		this.maxTxn = maxTxn;
	}
}
