package com.codingraja.domain;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("saving_account")
public class SavingAccount extends Account {
	@Column(name="inrest_rest")
	private Double intrestRate;
	
	public SavingAccount(){}
	public SavingAccount(Double balance, long customerId, Double intrestRate) {
		super(balance, customerId);
		this.intrestRate = intrestRate;
	}
	
	public Double getIntrestRate() {
		return intrestRate;
	}
	public void setIntrestRate(Double intrestRate) {
		this.intrestRate = intrestRate;
	}
}
