package com.codingraja.domain;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("loan_account")
public class LoanAccount extends Account {
	@Column(name="inrest_rest")
	private Double intrestRate;
	@Column(name="emi")
	private Double emi;
	@Column(name="out_std_amount")
	private Double outStdAmount; //outstanding Amount
	
	public LoanAccount(){}
	public LoanAccount(Double balance, long customerId, Double intrestRate, Double emi, Double outStdAmount) {
		super(balance, customerId);
		this.intrestRate = intrestRate;
		this.emi = emi;
		this.outStdAmount = outStdAmount;
	}
	
	public Double getIntrestRate() {
		return intrestRate;
	}
	public void setIntrestRate(Double intrestRate) {
		this.intrestRate = intrestRate;
	}
	public Double getEmi() {
		return emi;
	}
	public void setEmi(Double emi) {
		this.emi = emi;
	}
	public Double getOutStdAmount() {
		return outStdAmount;
	}
	public void setOutStdAmount(Double outStdAmount) {
		this.outStdAmount = outStdAmount;
	}
}
